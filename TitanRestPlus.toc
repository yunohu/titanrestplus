## Interface: 100002
## Title: Titan Panel [|cffeda55fRest+|r] |cff00aa0010.0.0.0|r
## Notes: Keeps track of the RestXP amounts and status for all of your characters.
## Author: Current - Yonohu/Kernigha, Vogonjeltz and Madysen
## Author: GrayElf, Maillen, Vogonjeltz, Madysen, Yonohu/Kernighan
## DefaultState: Enabled
## SavedVariables: RestPlus_Data, RestPlus_Settings, RestPlus_Colors
## OptionalDeps:
## Dependencies: Titan
## Version: 10.0.0.0
## X-Category: Interface Enhancements
## X-Date: 2022-12-5
## X-Child-of: Titan
TitanRestPlus.xml
